# Configuracion del repo

En Settings -> General -> Merge request approvals

"Approvals required" subirlo a la cantidad de personas que queremos que revisen cada pull request del repo (es una configuracion general para todo el repo, y se hace una sola vez):

![alt text for screen readers](./img/0-cambio-en-el-repo-por-unica-vez.png "Configuración")


# Uso de parte de los devs

### Paso 1:
En la pantalla luego de hacer un merge request, nos aparece la opción de agregar un reviewer al codigo, es opcional llenar este campo ya que cualquier miembro del repositorio con rol de al menos "Developer" puede aprobar un merge request, la ventaja de si se agrega un reviewer en este paso es que le va a llegar un mail avisandole.
Luego se procede normalmente, creando el merge request.

![alt text for screen readers](./img/1-mergerequest-reviewer-se-agrega-si-se-quiere-sino-no-hace-falta-cualquiera-con-dev-puede-luego-create-merge.png "Tooltip")

### Paso 2 (Vista desde el revisor del codigo)
Si lo agregaron como revisor se le enviara un mail, sino, se puede chequear en el menu de la izquierda en la seccion de "merge requests" dentro elegimos el MR con el que queremos trabajar, al abrirlo aparecera la opción para aprobar el MR.
El aprobado permite que se pueda seguir con el merge normalmente pero no realiza el merge en si, solo da la aprobación de que todo esta bien.

![alt text for screen readers](./img/3-LR-aprobar.png "Tooltip")

Si ningun revisor aprobó el merge request no permitira mergear:

![alt text for screen readers](./img/8sinaprobacionnodejamergear.png "Tooltip")

Luego de revisar el codigo en la pestaña "changes", el revisor puede aprobar el merge request, y al dev original (y a todos los que tengan los permisos para hacer merges) se le activará la opción para mergear (vista desde un dev con permisos de MR):

![alt text for screen readers](./img/9conaprobaciondejamergear.png "Tooltip")

Si fuese necesario, el dev que dio la aprobación la puede revocar clickeando en el mismo boton, siempre y cuando no se haya realizado el merge.

![alt text for screen readers](./img/3a-LR-aprobado.png "Tooltip")

Notas:

- El dev que realizó el Merge request no tiene permisos para aprobar su propio MR

- Si se realizan commits nuevos a un MR ya aprobado (puede ser el caso de que mas de 1 persona necesite aprobar y una la apruebe pero la otra encuentre algo que necesita corregirse), los aprobados anteriores se resetean.


# Agregar comentarios al codigo en la revisión

Es muy util agregar comentarios al hacer la revisión, ya que quedan asentados y uno puede volver a mirarlos y darle una respuesta al revisor, se puede elegir el archivo y especificamente a que lineas de codigo nos estamos refiriendo.
Para ello se hace click en la parte izquierda de la linea de codigo a la que nos queremos referir:

![alt text for screen readers](./img/5a-agregarcommentalcodigo.png "Tooltip")

Se nos abrira un cuadro de texto para rellenar con el comentario.

![alt text for screen readers](./img/5b-agregarcommentalcodigo-addcomment.png "Tooltip")

Luego de rellenar con el comentario se hace click en "Add comment now" ("Start a Review" es parecido pero necesita un click extra).

![alt text for screen readers](./img/6agregarcommentalcodigo-addcomment.png "Tooltip")

El dev original puede responder a los comentarios hechos por el revisor y marcar el comentario como "resuelto".

![alt text for screen readers](./img/6agregarcommentalcodigo-sepuederesponderymarcarcomosolucionado.png "Tooltip")
